<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
        <!-- IMPORTAR LA LIBRERIA DE JSTL TAGS core -->  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
   <%@ page import="java.util.*, java.sql.*, cine.practica1718.*" %>
   
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Practica Tecnologias Web 17/18</title>

<style type="text/css">

li{
	display: inline;
	padding: 10px;
	
}

body{
	background-color: #FFC;
	text-align: center;
}
nav{
	margin: 0 auto;
	text-align: center;
	margin-bottom:20px;
	
}

section{
	background-color: #FF6;
	margin: auto;
	text-align: center;
	
}
aside{
	float:left;
	background-color:#FC6;
}

footer{
	 	position: static;
  		bottom: 0;
  		margin: 0 auto;
}

</style>

</head>
<body>
	
         	<header>
      		<h1>PORTAL DE CINE</h1>
        	</header>
        	
        	<input type="hidden" name="instruccion" value="cliente">
        	
            
            <nav>
        	<!--Listas de elementos desordenadas(UL)-->
            	<ul>
            	<c:url var="compras" value="ManagerCine">
			 	<c:param name="instruccion" value="comprasEntradas"></c:param>	
				</c:url>
        	    <c:url var="puntuacion" value="ManagerCine">
			 	<c:param name="instruccion" value="puntuarPelicula"></c:param>	
				</c:url>
        	    <c:url var="salir" value="ManagerCine">
			 	<c:param name="instruccion" value="salirLogin"></c:param>	
				</c:url>
            		<li><a href="quienessomosVistaCliente.jsp">Quienes somos</a></li>                	
                	<li><a href="${salir}">Salir</a></li>
					<li><%= session.getAttribute("usuario") %></li>
					
            	</ul>	
       	      
            </nav>
            
			<section>
			
			
            
             <aside>
             
             	<c:url var="compras" value="ManagerCine">
			 	<c:param name="instruccion" value="comprasEntradas"></c:param>	
				</c:url>
        	    <c:url var="puntuacion" value="ManagerCine">
			 	<c:param name="instruccion" value="puntuarPelicula"></c:param>	
				</c:url>
            <blockquote><a href="${compras}">Comprar Entradas<a></a></blockquote>
            <blockquote><a href="${puntuacion}">Puntuar Pelicula</a></blockquote>
            <blockquote><a href="accesoBaja.jsp">Darse de Baja</a></blockquote>
        	
       		 </aside>
			
				<table>
		
					<tr>	<!-- Representacion de la primera fila de una tabla con la etiqueta <tr> -->
						<td class="cabecera">ID</td>
						<td class="cabecera">NOMBRE</td>
						<td class="cabecera">PUBLICACION</td>
						<td class="cabecera">PUNTACION</td>

					</tr>
	

				<c:forEach var="temp" items="${LISTAPELI}">
					
				<c:url var="linktem" value="ManagerCine">
			 	<c:param name="instruccion" value="puntos"></c:param>
			 	<c:param name="id" value="${temp.id}"></c:param>	
				</c:url>
					
				
					<tr><!-- Representacion de otra fila-->
					<td class="filas">${temp.id}</td>
					<td class="filas">${temp.nombre}</td>
					<td class="filas">${temp.aņopublicacion}</td>
					<td class="filas">${temp.puntos}</td>
					<td class="filas"><a href="${linktem}">Puntuar</a></td>	
					</tr>
		
				</c:forEach>
	
				</table>

            </section>

        <footer><small>Derechos Reservados</small> <address>666666666</address></footer>

</body>
</html>