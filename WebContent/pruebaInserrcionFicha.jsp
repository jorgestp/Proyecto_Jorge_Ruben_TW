<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
        <!-- IMPORTAR LA LIBRERIA DE JSTL TAGS core -->  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
   <%@ page import="java.util.*, java.sql.*, cine.practica1718.*" %>
   
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Practica Tecnologias Web 17/18</title>

<style type="text/css">

li{
	display: inline;
	padding: 10px;
	
}

body{
	background-color: #FFC;
	text-align: center;
}
nav{
	margin: 0 auto;
	text-align: center;
	margin-bottom:20px;
	
}

section{
	background-color: #FF6;
	margin: auto;
	text-align: center;
}
aside{
	float:left;
	background-color:#FC6;
}

footer{
	position: absolute;
	bottom: -2px;
	margin: auto;
	left: 509px;
}

.boton {
	text-align: center;
}
</style>

</head>
<body>
	
	
         	<header>
      		<h1>PORTAL DE CINE</h1>
        	</header>
        	
        	
            
            <nav>
        	<!--Listas de elementos desordenadas(UL)-->

            	<ul>        		
        	    <c:url var="salir" value="ManagerCine">
			 	<c:param name="instruccion" value="salirLogin"></c:param>	
				</c:url>
            	<c:url var="vueltaInicioAdmin" value="ManagerCine">
			 		<c:param name="instruccion" value="vueltaInicioAdmin"></c:param>	
					</c:url>
					<li><a href="${vueltaInicioAdmin}">Inicio</a></li> 
            		<li><a href="quienessomosVistaAdministrador.jsp">Quienes somos</a></li>
                	<li><a href="${salir}">Salir</a></li>
					<li><%= session.getAttribute("usuario") %></li>
            	</ul>	
       	      
            </nav>
            
			<section>
			
			

             <aside>
             
             
             	<c:url var="pelis" value="ManagerCine">
			 	<c:param name="instruccion" value="formalizarInserccionPelicula"></c:param>	
				</c:url>
        	    <c:url var="salas" value="ManagerCine">
			 	<c:param name="instruccion" value="salas"></c:param>	
				</c:url>
        	    <c:url var="precio" value="ManagerCine">
			 	<c:param name="instruccion" value="precio"></c:param>	
				</c:url>
            <blockquote><a href="${pelis}">A�adir Peliculas<a></a></blockquote>
            <blockquote><a href="${salas}">Configurar Salas</a></blockquote>
            <blockquote><a href="${precio}">Configurar Precio</a></blockquote>
            
        	
       		 </aside>
			
			

	<input type="hidden" name="instruccion" value="insertaPelicula">
  
		
	<h1 style="text-align:center">FICHA</h1>

	
	<h2 style="color:blue;"> PELICULA INSERTADA CORRECTAMENTE</h2>
            
	</section>

<footer><small>Derechos Reservados</small> <address>666666666</address></footer>


</body>
</html>