<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
        <!-- IMPORTAR LA LIBRERIA DE JSTL TAGS core -->  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
   <%@ page import="java.util.*, java.sql.*, cine.practica1718.*" %>
   
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Practica Tecnologias Web 17/18</title>

<style type="text/css">

li{
	display: inline;
	padding: 10px;
	
}

body{
	background-color: #FFC;
	text-align: center;
}
nav{
	margin: 0 auto;
	text-align: center;
	margin-bottom:20px;
	
}

}
aside{
	float:left;
	background-color:#FC6;
}




#tablaExito{

	background:#00C;
	padding:10px;
	border:solid 2px #FF0000;
	text-align: center;
	margin: 0 auto;

}

</style>

</head>
<body>
			
	
         	<header>
      		<h1>PORTAL DE CINE</h1>
        	</header>
        	
        	
        	
            
            <nav>
        	<!--Listas de elementos desordenadas(UL)-->
            	<ul>
            		<c:url var="vueltaInicioCliente" value="ManagerCine">
			 		<c:param name="instruccion" value="vueltaInicioCliente"></c:param>	
					</c:url>
					<li><a href="${vueltaInicioCliente}">Inicio</a></li>
                	<li><a href="#">Salir</a></li>

					<li><%= session.getAttribute("usuario") %></li>
            	</ul>	
       	      
            </nav>
            
			<section>
			
			
            
             <aside>
             
             	<c:url var="compras" value="ManagerCine">
			 	<c:param name="instruccion" value="comprasEntradas"></c:param>	
				</c:url>
        	    <c:url var="puntuacion" value="ManagerCine">
			 	<c:param name="instruccion" value="puntuarPelicula"></c:param>	
				</c:url>
            <blockquote><a href="${compras}">Comprar Entradas<a></a></blockquote>
            <blockquote><a href="${puntuacion}">Puntuar Pelicula</a></blockquote>
             <blockquote><a href="accesoBaja.jsp">Darse de Baja</a></blockquote>
        	
       		 </aside>
             
             <section>
             
        <table width="276" align="center" cellpadding="1">
    		<tr>
      			<td width="13%"><label for="id">ID:</label></td>
      			<td width="87%"><label id="id">${pelicula.id}</label></td>
    		</tr>
    		<tr>
     			<td><label for="nombre">Nombre:</label></td>
      			<td><label id="nombre">${pelicula.nombre}</label></td>
    		</tr>
    		<tr>
      			<td><label for="publicacion">Publicacion:</label></td>
      			<td><label id="publicacion">${pelicula.aņopublicacion}</label></td>
    		</tr>
    		<tr>
      			<td><label for="publicacion">Butacas Libres:</label></td>
      			<td><label id="publicacion">${butacas}</label></td>
    		</tr>
    		<tr>
      			<td><label for="publicacion">Puntuacion:</label></td>
      			<td><label id="publicacion">${pelicula.puntos}</label></td>
    		</tr>
    		<tr>  
  		</table>           

  <form action="ManagerCine" method="get">
  
  	<input type="hidden" name="instruccion" value="formalizarCompra">
  	<input type="hidden" name="id" value="${pelicula.id}">     	
    <p>Numero de entradas a comprar:</p> 
    <label>
      <input type="radio" name="articulos" value="1" >
      1</label>
    &nbsp;
    <label>
      <input type="radio" name="articulos" value="2" >
    2 </label>
    &nbsp;
    <label>
      <input type="radio" name="articulos" value="3" >
      3 </label>
    &nbsp;
    <label>
      <input type="radio" name="articulos" value="4" >
    4 </label>
    &nbsp;
    <label>
      <input type="radio" name="articulos" value="5" >
    5 </label>
      &nbsp;
    <label>
      <input type="radio" name="articulos" value="6" >
    6 </label>
    <label>
      <input type="radio" name="articulos" value="7" >
    7 </label>
    <label>
      <input type="radio" name="articulos" value="8" >
    8 </label>
  
  	<br>
    <br>
    <input type="submit" name="button" id="button" value="Enviar" >
    </p>
    </form>
    <table id="tablaExito" width="200" border="1">
      <tr>
        <td> <h3 style="color: #F00;"> ERROR EN LA COMPRA!</h3></td>
      </tr>
      <label style="color:#F00"> No quedan ${seleccion} butacas libres... </label>
    </table>
    <br>

  
</section>

<footer><small>Derechos Reservados</small> <address>666666666</address></footer>

</body>
</html>