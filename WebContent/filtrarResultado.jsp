<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
        <!-- IMPORTAR LA LIBRERIA DE JSTL TAGS core -->  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
   <%@ page import="java.util.*, java.sql.*, cine.practica1718.*" %>
   
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Practica Tecnologias Web 17/18</title>

<style type="text/css">

li{
	display: inline;
	padding: 10px;
}

body{
	background-color: #FFC;
	margin: 0 auto;
	text-align: center;
}
nav{
	margin: 0 auto;
	text-align: center;
}

#lista{

	background-color: #FF6;
	text-align: center;
	margin: 0 auto;

}
aside{
	float:left;
	
}

.left{
	float:left;
}
.right{
	float:right;
}
footer{
	float:inherit;
}

</style>

</head>
<body>
	
         	<header>
      		<h1>PORTAL DE CINE</h1>
        	</header>
        	
        	<input type="hidden" name="instruccion" value="invitado">
        	
            
            <nav>
        	<!--Listas de elementos desordenadas(UL)-->
           	  <ul>
            	<ul>
            		<li><a href="quienessomos.jsp">Quienes somos</a></li>
                	<li><a href="promociones.jsp">Promociones</a></li>
                	<li><a href="filtrar.jsp">Filtrar Peliculas</a></li>
                	<li><a href="login.jsp">Login/Registrate</a></li>
            	</ul>
           	  </ul>
 
            </nav>
            <div class="left">
                   	      <form name="form1" method="get" action="ManagerCine">
       	      
       	      <input type="hidden" name="instruccion" value="filtrar">
       	        <table width="357" border="1" cellspacing="1">
       	          <tr>
       	            <th width="93" scope="row">CATEGORIA</th>
       	            <td width="251"><label>
        				<select name="categoria" size="1" id="categoria">
          					<option>Comedia</option>
                            <option selected>Categoria</option>
          					<option>Suspense</option>
          					<option>Terror</option>
          					<option>Animacion</option>
          					<option>C.Ficcion</option>
          					<option>Accion</option>
        				</select>
      						</label></td>
   	              </tr>
       	          <tr>
       	            <th scope="row">ACTOR</th>
       	            <td><label for="textfield"></label>
   	                <input type="text" name="actor" id="actor" value=""></td>
   	              </tr>
       	          <tr>
       	            <th scope="row">PUNTUACION</th>
       	            <td><label>
        				<select name="puntuacion" size="1" id="puntuacion">
          					<option value="5">Menos de 5</option>
                            <option value="0" selected>0</option>
          					<option value="6">Entre 5 y 7.5</option>
          					<option value="8">Mas de 7.5</option>
        				</select>
      						</label></td>
   	              </tr>
                  <tr>
                  <th></th>
                  <td> 
                  <input type="submit" name="enviar" id="enviar" value="Enviar">
                  </td>
                  </tr>
   	            </table>
   	          </form>
            
            </div>
<div class="right">
                        				<table width="492" border="1">
            				  <tr>
            				    <th width="64" scope="col">ID</th>
            				    <th width="207" scope="col">NOMBRE</th>
            				    <th width="126" scope="col">PUBLICACION</th>
            				    <th width="67" scope="col">&nbsp;</th>
          				    </tr>
                            <c:forEach var="temp" items="${LISTAPELI}">
							<c:url var="linktem" value="ManagerCine">
			 				<c:param name="instruccion" value="verficha"></c:param>
			 				<c:param name="id" value="${temp.id}"></c:param>	
							</c:url>
				
							<tr><!-- Representacion de otra fila-->
								<td class="filas">${temp.id}</td>
								<td class="filas">${temp.nombre}</td>
								<td class="filas">${temp.aņopublicacion}</td>
								<td class="filas"><a href="${linktem}">Ver Ficha</a></td>	
							</tr>
		
							</c:forEach>
          				  </table>
            
</div>
           

<footer>
 

  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p><small>Derechos Reservados</small> </p>
<address>666666666</address></footer>

</body>
</html>