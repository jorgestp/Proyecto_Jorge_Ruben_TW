<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
        <!-- IMPORTAR LA LIBRERIA DE JSTL TAGS core -->  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
   <%@ page import="java.util.*, java.sql.*, cine.practica1718.*" %>
   
   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Practica Tecnologias Web 17/18</title>

<style type="text/css">

li{
	display: inline;
	padding: 10px;
	
}

body{
	background-color: #FFC;
	text-align: center;
}
nav{
	margin: 0 auto;
	text-align: center;
	margin-bottom:20px;
	
}

section{
	background-color: #FF6;
	margin: auto;
	text-align: center;
	
}
aside{
	float:left;
	background-color:#FC6;
}

footer{
	 	position: absolute;
  		bottom: 0;
  		margin:auto
}

</style>

</head>
<body>
	
         	<header>
      		<h1>PORTAL DE CINE</h1>
        	</header>
        	
        	
            
            <nav>
        	<!--Listas de elementos desordenadas(UL)-->

            	<ul>        		
        	    <c:url var="salir" value="ManagerCine">
			 	<c:param name="instruccion" value="salirLogin"></c:param>	
				</c:url>
				<c:url var="vueltaInicioAdmin" value="ManagerCine">
			 		<c:param name="instruccion" value="vueltaInicioAdmin"></c:param>	
					</c:url>
					<li><a href="${vueltaInicioAdmin}">Inicio</a></li> 
            	
            		<li><a href="quienessomosVistaAdministrador.jsp">Quienes somos</a></li>
                	<li><a href="${salir}">Salir</a></li>
					<li><%= session.getAttribute("usuario") %></li>
            	</ul>	
       	      
            </nav>
            
			<section>
			
			
            
             <aside>
             
             
             	<c:url var="pelis" value="ManagerCine">
			 	<c:param name="instruccion" value="formalizarInserccionPelicula"></c:param>	
				</c:url>
        	    <c:url var="salas" value="ManagerCine">
			 	<c:param name="instruccion" value="salas"></c:param>	
				</c:url>
        	    <c:url var="precio" value="ManagerCine">
			 	<c:param name="instruccion" value="precio"></c:param>	
				</c:url>
            <blockquote><a href="${pelis}"> Aņadir Peliculas </a> </blockquote>
            <blockquote><a href="${salas}">Configurar Salas</a></blockquote>
            <blockquote><a href="${precio}">Configurar Precio</a></blockquote>
            

             
             </aside>
             
        <table width="276" align="center" cellpadding="1">
    		<tr>
      			<td width="13%"><label for="id">ID:</label></td>
      			<td width="87%"><label id="id">${pelicula.id}</label></td>
    		</tr>
    		<tr>
     			<td><label for="nombre">Nombre:</label></td>
      			<td><label id="nombre">${pelicula.nombre}</label></td>
    		</tr>
    		<tr>
      			<td><label for="publicacion">Publicacion:</label></td>
      			<td><label id="publicacion">${pelicula.aņopublicacion}</label></td>
    		</tr>  
  		</table>           

  <form action="ManagerCine" method="get">
  
  	<input type="hidden" name="instruccion" value="formalizarSesiones">
  	<input type="hidden" name="id" value="${pelicula.id}">     	
    <p>INSERTE LAS SESIONES DE LA PELICULA:</p> 
    <label>
      <input type="checkbox" name="articulos" value="12:00" >
      12:00</label>
    &nbsp;
    <label>
      <input type="checkbox" name="articulos" value="15:00" >
    15:00</label>
    &nbsp;
    <label>
      <input type="checkbox" name="articulos" value="18:00" >
      18:00</label>
    &nbsp;
    <label>
      <input type="checkbox" name="articulos" value="21:00" >
    21:00</label>
    &nbsp;
    <label>
      <input type="checkbox" name="articulos" value="00:00" >
    00:00</label>
  	<br>
    <br>
    <input type="submit" name="button" id="button" value="Enviar">
    </p>
    
    </form>
  
</section>

        <footer><small>Derechos Reservados</small> <address>666666666</address></footer>

</body>
</html>