/**
 * 
 */
package cine.practica1718.modelo.pelicula;



/**
 *  *  @author 47536486V y 72978510Q
 *
 */
public class Ficha {

	private String  descripcion, categoria, actor, director;
	private int id;

	/**
	 * Constructor para crear la ficha
	 * @param id identificador de la pelicula.
	 * @param descripcion Es la descripcion de la pelicula.
	 * @param categoria Clasificacion de la pelicula.
	 * @param actor Actor de la pelicula.
	 * @param director
	 */
	public Ficha(int id, String descripcion, String categoria, String actor, String director) {
		this.id = id;
		this.descripcion = descripcion;
		this.categoria = categoria;
		this.actor = actor;
		this.director = director;
	}

	/**
	 * Modificador del parametro id.
	 * @param id entero.
	 */
	public void setId( int id) {
		
		this.id = id;
	}
	
	/**
	 * recupera el parametro id.
	 * @return
	 */
	public int getId() {
		
		return id;
	}

	/**
	 * Recupera la descripcion de la pelicula. 
	 * @return
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Modificador para la descripcion de la pelicula.
	 * @param descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Recupera la categoria.
	 * @return
	 */
	public String getCategoria() {
		return categoria;
	}

	/**
	 * Modifica la categoria.
	 * @param categoria
	 */
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	/**
	 * Recupera el campo actor.
	 * @return
	 */
	public String getActor() {
		return actor;
	}

	/**
	 * modifica el campo actor.
	 * @param actor
	 */
	public void setActor(String actor) {
		this.actor = actor;
	}

	/**
	 * recupera campo el director.
	 * @return
	 */
	public String getDirector() {
		return director;
	}

	/**
	 * modifica el campo director.
	 * @param director
	 */
	public void setDirector(String director) {
		this.director = director;
	}
	
	
	
}
