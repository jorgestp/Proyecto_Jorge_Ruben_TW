package cine.practica1718.modelo.pelicula;

/**
 * Clase que simula una sesion del cine.
 * @author 47536486V y 72978510Q
 *
 */
public class Sesion {
	
	private int id, butacas;
	private String pase;
	
	
	/**
	 * Constructor de la pelicula.
	 * @param id
	 * @param pase
	 * @param butacas
	 */
	public Sesion (int id, String pase, int butacas) {
		
		this.id = id;
		this.pase = pase;
		this.butacas = butacas;
	}
	
	
	/**
	 * recupera el id.
	 * @return id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * modifica el campo id.
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * recupera el numero de butacas.
	 * @return
	 */
	public int getButacas() {
		return butacas;
	}
	
	/**
	 * modifica el numero de butacas.
	 * @param butacas
	 */
	public void setButacas(int butacas) {
		this.butacas = butacas;
	}
	
	/**
	 * recupera el pase de la pelicula.
	 * @return
	 */
	public String getPase() {
		return pase;
	}
	
	/**
	 * modifica el pase de la pelicula.
	 * @param pase
	 */
	public void setPase(String pase) {
		this.pase = pase;
	}
	
	

}
