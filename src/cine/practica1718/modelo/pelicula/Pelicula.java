/**
 * 
 */
package cine.practica1718.modelo.pelicula;

/**
 *  *  @author 47536486V y 72978510Q
 * Clase que representa una pelicula del cine.
 */
public class Pelicula {
	
	 private String nombre;
	 private int añopublicacion; //representa el año en el que la pelicula se estren�				 
	 private int id;
	 private double puntos;
	 private int sala;
	 private double precio;


	 /**
	  * Constructor de la pelicula.
	  * @param nombre -- es un string,  nombre de la pelicua.
	  * @param añopublicacion -- es un entero con el año de publicacion-
	  */
	public Pelicula(String nombre, int añopublicacion) {

		this.nombre = nombre;
		this.añopublicacion = añopublicacion;
	}
	 
	/**
	 * Constructor de la pelicula, se pasan 5 parametros.
	 * @param id identificador de la pelicula.
	 * @param nombre nombre de la pelicula.
	 * @param añopublicacion año de publicacion de la pelicula.
	 * @param puntos puntos obtenidos por los usuarios .
	 * @param sala sala en la que se emite la pelicula.
	 * @param precio precio de la pelicula.
	 */
	 public Pelicula(int id, String nombre, int añopublicacion, double puntos,int sala, double precio) {
		 	
		 	this.id = id;
		 	this.nombre = nombre;
			this.añopublicacion = añopublicacion;
			
			this.puntos = puntos;
			this.sala = sala;
			this.precio = precio;
	 }


	/**
	 * recupera los puntos de la pelicula.
	 * @return devuelve los puntos.
	 */
	public double getPuntos() {
		return puntos;
	}

	/**
	 * modifica los puntos de la pelicula.
	 * @param puntos
	 */
	public void setPuntos(double puntos) {
		this.puntos = puntos;
	}

	/**
	 * recupera el nombre de la pelicula.
	 * @return devuelve el nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * modifica el nombre de la pelicula.
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	/**
	 * recupera el año de publicacion de la pelicula.
	 * @return
	 */
	public int getAñopublicacion() {
		return añopublicacion;
	}

	/**
	 * modifica el año de publicacion de la pelicula.
	 * @param añopublicacion
	 */
	public void setAñopublicacion(int añopublicacion) {
		this.añopublicacion = añopublicacion;
	}

	 /**
	  * recupera el id de la pelicula.
	  * @return
	  */
	 public int getId() {
		return id;
	}

	/**
	 * modifica el id de la pelicula.
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	
	/**
	 * recupera la sala en la que se emite la pelicula.
	 * @return
	 */
	 public int getSala() {
		return sala;
	}

	 /**
	  * modifca el campo en el que se emite la pelicula..
	  * @param sala
	  */
	public void setSala(int sala) {
		this.sala = sala;
	}
	
	/**
	 * recupera el precio.
	 * @return
	 */
	public double getPrecio() {
		return precio;
	}

	/**
	 * modifica el precio.
	 * @param precio
	 */
	public void setPrecio(double precio) {
		this.precio = precio;
	}

}
