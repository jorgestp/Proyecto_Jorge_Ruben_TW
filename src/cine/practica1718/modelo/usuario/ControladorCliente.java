package cine.practica1718.modelo.usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Clase que controla la gestion de clientes.
 * @author Ruben
 *
 */
public class ControladorCliente {
	
	private Connection conexion;
	
	
	/**
	 * Constructor , se le pasa una conexion.
	 * @param conexion
	 */
	public ControladorCliente(Connection conexion) {
		
		this.conexion = conexion;
	}

	/**
	 * metodo que agrega un cliente.
	 * @param cliente
	 * @return
	 */
	public boolean agregaCliente(Cliente cliente) {
				
		PreparedStatement statepreparada = null;
		try {
			
			String sql ="INSERT INTO login (USUARIO, PASS, NOMBRE, APELLIDO) VALUES (?,?,?,?)";
			
			statepreparada = conexion.prepareStatement(sql);
			
			statepreparada.setString(1, cliente.getUser());
			statepreparada.setString(2, cliente.getPassword());
			statepreparada.setString(3, cliente.getNombre());
			statepreparada.setString(4, cliente.getApellido());
						
			
			statepreparada.executeUpdate();
			return true;
			
			
		}catch (Exception e) {
			
			e.printStackTrace();
			return false;
		}
		
	}

	/**
	 * Metodo que permite acceder un usuario al sistema.
	 * @param cliente
	 * @return
	 */
	public Cliente accedeCliente(Cliente cliente) {

		PreparedStatement statepreparada = null;
		Cliente c= null;
		
		try {
			
			String sql ="SELECT * FROM login where	usuario=? and pass=?";
			
			statepreparada = conexion.prepareStatement(sql);
			
			statepreparada.setString(1, cliente.getUser());
			statepreparada.setString(2, cliente.getPassword());

			ResultSet rs = statepreparada.executeQuery();
			
			
			
				if(rs.next()) {
					
					//Las columnas de la tabla LOGIN estan repartidas; 1� USER 2� PSS 3� NOMBRE 4� Apellido
					// new cliente ( nombre, apellido, user, pass)
					c = new Cliente(rs.getString(3), rs.getString(4), rs.getString(1), rs.getString(2));
				}
			
			
			
			return c;
			
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return c;
	}

	/**
	 * Metodo para dar de baja un cliente del sistema.
	 * @param user
	 * @param pass
	 */
	public void darBajaCliente(String user, String pass) {
		
		PreparedStatement state = null;
		
		String sql = "DELETE FROM login WHERE usuario=? and pass=?";
		
		try {
			state = conexion.prepareStatement(sql);
			state.setString(1, user);
			state.setString(2, pass);
			
			state.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	

}
