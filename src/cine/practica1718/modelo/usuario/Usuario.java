package cine.practica1718.modelo.usuario;
/**
 * Clase abstracta, representa un usuario de la aplicacion
 *  @author 47536486V y 72978510Q
 *
 */
public abstract class Usuario {

    private String nombre, apellido, user, password;

    /**
     * Constructor de un nuevo usuario.
     * @param nombre
     * @param apellido
     * @param user
     * @param password
     */
	public Usuario(String nombre, String apellido, String user, 
			String password) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.user = user;
		this.password = password;
		
	}
	
	/**
	 * Constructor de un usuario.
	 * @param user
	 * @param pss
	 */
	public Usuario(String user, String pss) {
		
		this.user = user;
		
		this.password=pss;
		
	}

	/**
	 * recupera el nombre del usuario.
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * modifica el nombre del usuario.
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * recupera el apellido
	 * @return
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * modifica el apellido.
	 * @param apellido
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * recupera el nombre de usuario.
	 * @return
	 */
	public String getUser() {
		return user;
	}

	/**
	 * modifca el nombre de usaurio.
	 * @param user
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * recupera el password.
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * modifica el password.
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	
	
}
