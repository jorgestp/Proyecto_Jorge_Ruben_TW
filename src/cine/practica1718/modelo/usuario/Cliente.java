package cine.practica1718.modelo.usuario;

/**
 * 
 *  @author 47536486V y 72978510Q
 *
 *Clase que representa un cliente registrado en la BBDD
 */
public class Cliente extends Usuario {

	/**
	 * Constructor de un nuevo cliente.
	 * @param nombre
	 * @param apellido
	 * @param user
	 * @param password
	 */
	public Cliente(String nombre, String apellido, 
			String user, String password) {
		
		super(nombre, apellido, user, password);

	}
	
	/**
	 * Constructor de un nuevo cliente.
	 * @param user
	 * @param pss
	 */
	public Cliente(String user, String pss) {
		
		super(user, pss);
	}

	
}
